/**
 * SPDX-PackageName: kwaeri/renderer
 * SPDX-PackageVersion: 0.3.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as http from 'http';
import * as fs from 'fs';
import { kdt } from '@kwaeri/developer-tools';
import debug from 'debug';


// DEFINES
const _ = new kdt(),
      DEBUG = debug( 'nodekit:renderer' );


export class Renderer {
    /**
     * @var { boolean } xrm
     */
    public xrm: boolean;


    /**
     * @var { boolean } ssr
     */
    public ssr: boolean;


    /**
     * @var { Utility } utilities
     */
    public utilities;


    /**
     * @var { string } derivedViewPath
     */
    public derivedViewPath: string;


    /**
     * @var { string } derivedExtensionViewPath
     */
    public derivedExtensionViewPath: string;


    /**
     * @var { string } layout
     */
    public layout: string;


    /**
     * @var { Date } date
     */
    public date: Date;


    /**
     * Class constructor
     */
    constructor( utilities: any, derivedViewPath: string, xrm: boolean, ssr: boolean, layout: any, derivedExtensionViewPath: string ) {
        this.utilities                  = new utilities.html();
        this.derivedViewPath            = derivedViewPath;
        this.derivedExtensionViewPath   = derivedExtensionViewPath;
        this.xrm                        = xrm;
        this.ssr                        = ssr;
        this.layout                     = layout || null;
        this.date                       = new Date();
    }


    /**
     * Fetches the layout content so it can be parsed
     *
     * @param request
     * @param response
     * @param klay
     */
    turn( request: any, response: any, klay: any, isExtension: boolean, extData: any, current: any ) {
        // Prepare the derived view path and a copy of 'this' for later use;
        let viewPath                = this.derivedViewPath,
            extensionPath           = this.derivedExtensionViewPath,
            isExtensionCall         = false,
            processingModuleContent = false,
            extensionController     = false,
            extensionVendor         = false,
            extension               = false,
            extensionView           = false,
            processor               = this;

        // We'll need to modify paths if this is an extension call
        if( isExtension ) {
            isExtensionCall = true;

            if( extData.hasOwnProperty( 'vendor' ) && extData.hasOwnProperty( 'extension' ) ) {
                extensionVendor = extData.vendor;
                extension = extData.extension;
            }

            if( extData.hasOwnProperty( 'isModuleContent' ) && extData.isModuleContent )
                processingModuleContent = true;
        }

        // We'll check if this application is an xrm or server-side render
        // project
        //
        // If so we want to force the use of a layout
        // but only if it wasn't manually overridden.
        //
        // TODO: EVALUATE
        if( ( this.xrm || this.ssr ) && !klay.layoutOverride ) {
            if( !klay.hasOwnProperty( 'layout' ) ) {
                // If a layout wasn't specified, use the default
                // for the currently configured theme
                klay.layout = this.layout + '/main.kml';
            }
            else {
                // Otherwise, let's go ahead and use the specified
                // layout from the currently configured theme.
                if( !klay.layout || klay.layout === '' ) {
                    // Set it to the default, as it was not overridden yet
                    // was set to false or an empty string.
                    klay.layout = this.layout + '/main.kml';
                }
                else {
                    // It contained a value, so prepare it accordingly.
                    klay.layout = this.layout + '/' + klay.layout + '.kml';
                }
            }

            // We also need to load any additionally required scripts, i.e. from
            // extensions and sorts

        }

            // If the value was overridden, we want to override the use of the
            // configured theme. Let's leave everything as it is, so that the
            // supplied value is not disturbed.
            //
            // If it was false, use of a layout will be skipped, if a string
            // value was supplied it will be used as the string path to the
            // specified layout.

            // TODO: EVALUATE:
            //
            // If the app is not flagged as an xrm or with ssr, then there should
            // not be a view system in use - and as such layouts would similarly
            // be non-existent. If either application type is flagged and layouts
            // are overriden then we do not want layouts used for that case, and
            // when layouts are specified they should be drawn from the theme
            // path as action view files are used in conjunction with them, and so
            // we should never be searching for layouts from there (view/action
            // path) outside of partials, which have their own mechanism for use.
            //
            // Thus the below logic should not be needed, and only the ssr flag
            // needed to be included, and the existing flow cleaned up.
            //
            // ?? But if its not flagged for xrm or server-side rendering, we need
            // a path to the layout specified, if one was  - and the default
            // should be the controller's view directory:
            //if( !this.xrm || !this.ssr ) {
            //    if( klay.layout && klay.layout !== '' )
            //        klay.layout = viewPath + '/' + klay.controller + '/' + klay.layout + '.kml';
            //}

        // Check if a layout is specified after logic above has been executed, if so
        // we'll fetch said layout and process it for rendering the response.
        if( klay.layout && !processingModuleContent ) {
            fs.readFile(
                klay.layout,
                'utf8',
                ( error, ldata ) => {
                    let buffer;

                    // If there is an error
                    if( error ) {
                        // Log it
                        console.log( 'Error reading layout: ' + klay.layout );

                        // And set content to contain an error message
                        buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad...</h1><p>There was an issue loading the requested layout!</p></body></html>";

                        response.statusCode = 200;
                        response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                        response.setHeader( 'Content-Length', buffer.length );
                        response.write( buffer );
                        response.end();
                    }
                    else {
                        // Success
                        // There should always be a body
                        let derivedPath = '';
                        if( isExtensionCall )
                            derivedPath = extensionPath + '/' + extensionVendor + '/' + extension + '/views/' + klay.controller + '/' + klay.view + '.kml';
                        else
                            derivedPath = viewPath + '/' + klay.controller + '/' + klay.view + '.kml';

                        fs.readFile(
                            derivedPath,
                            'utf8',
                            ( error, vdata ) => {
                                let buffer;

                                if( error ) {
                                    // Log it
                                    DEBUG( `Error reading ${(isExtensionCall)?'extension':''} view [%s]`, derivedPath );

                                    console.log( `Error reading ${(isExtensionCall)?'extension':''} view [%s]`, derivedPath );

                                    // And set content to contain an error message
                                    buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad???...</h1><p>There was an issue loading the requested view!</p></body></html>";

                                    response.statusCode = 200;
                                    response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                                    response.setHeader( 'Content-Length', buffer.length );

                                    //response.writeHead();
                                    response.write ( buffer );
                                    response.end();
                                }
                                else {
                                    //var pottr = nodakwaeri.klay();
                                    //pottr.turn( request, response, klay );
                                    klay.layout = ldata;
                                    klay.view = vdata;

                                    processor.shape( request, response, klay );
                                }
                            }
                        );
                    }
                }
            );
        }
        else {
            // There should always be a body
            let derivedPath = '';
            if( isExtensionCall )
                derivedPath = extensionPath + '/' + extensionVendor + '/' + extension + '/views/' + klay.controller + '/' + klay.view + '.kml';
            else
                derivedPath = viewPath + '/' + klay.controller + '/' + klay.view + '.kml';

            fs.readFile(
                derivedPath,
                'utf8',
                ( error, vdata ) => {
                    let buffer;

                    if( error ) {
                        // Log it
                        DEBUG( `Error reading ${(isExtensionCall)?'extension':''} view [%s]`, derivedPath );

                        console.log( `Error reading ${(isExtensionCall)?'extension':''} view [%s]`, derivedPath );

                        // And set content to contain an error message
                        buffer = "<html><head><title>Failure</title></head><body><h1>Too Bad???...</h1><p>There was an issue loading the requested view!</p></body></html>";

                        if( processingModuleContent ) {

                            current._klay.module.rendered[current._klay.modules.contentMap[current._klay.modules.content[0]]] = 'Could not load module content';
                            current.next();

                            //processor.turn( request, response, klay, isExtension, extData );
                        }
                        else {
                            response.statusCode = 200;
                            response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                            response.setHeader( 'Content-Length', buffer.length );
                            //response.writeHead();
                            response.write ( buffer );
                            response.end();
                        }
                    }
                    else {
                        klay.layout = false;
                        klay.view = vdata;

                        if( processingModuleContent )
                            processor.shape( request, response, klay, true, current );
                        else
                            processor.shape( request, response, klay );
                    }
                }
            );
        }
    };


    /**
     * Pieces together - and sends - the response
     *
     * @param request
     * @param response
     * @param klay
     */
    shape( request: any, response: any, klay: any, isModuleContent: boolean = false, current: boolean =  false ) {
        let processor = this;
        if( klay )
            klay.authenticated = request.isAuthenticated;

        // Now let's prepare the body content; there may not be a layout, but if there is we need the body first.
        let body = klay.view;
        body = this.parse( body, klay );

        // If a layout was requested, let's prep it
        if( klay.layout ) {
            klay.view = body;
            let view = klay.layout;
            view = this.parse( view, klay );

            if( isModuleContent ) {
                console.log( 'Returning view with layout' );
                return ( current as any ).next( view );
            }
            else {
                response.statusCode = 200;
                response.setSession();
                response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                response.setHeader( 'Content-Length', view.length );
                response.write( view );
                response.end();
            }
        }
        else {
            if( isModuleContent ) {
                console.log( 'Returning body with out layout: ' );

                return ( current as any ).next( body );
            }
            else {
                response.statusCode = 200;
                response.setSession();
                response.setHeader( 'Content-Type', 'text/html; charset=utf-8' );
                response.setHeader( 'Content-Length', body.length );
                response.write( body );
                response.end();
            }
        }
    };


    /**
     * Parses kwaeri script
     *
     * @param content           Defines the content string to be parsed
     * @param klay              A reference to our klay object
     * @param iteration         Specifies which iteration is being invoked ( used for recursion )
     * @param identification    Specifies the identifier's name so we can properly reference it ( used for recursion )
     */
    parse( content: any, klay: any, iteration: boolean|number = false, identification: boolean|string = false ) {
        let processor = this;

        if( !iteration )
            iteration = false;

        if( !identification )
            identification = false;

        // Prepare some objects for a more comprehensible experience
        let brackets = {
            opening: '[[',
            closing: ']]',
            positions: {
                opening: [],
                closing: []
            }
        },
            bracketsCount = {
                opening: 0,
                closing: 0
            };

        /* Fill em up
        brackets.opening = '[[';
        brackets.closing = ']]';
        brackets.positions = {};
        bracketsCount.opening = 0;
        brackets.positions.opening = [];
        bracketsCount.closing = 0;
        brackets.positions.closing = [];
        */

        // First count the total number of opening brackets:
        let step = brackets.opening.length, // Should be the same length as closing
            pos = 0,
            run = true;

        while( run ) {
            pos = content.indexOf( brackets.opening, pos );
            if( pos >= 0 ) {
                ( brackets.positions.opening as any )[bracketsCount.opening] = pos;
                bracketsCount.opening++;
                pos += step;
            }
            else {
                run = false;
            }
        }

        // Then count the total number of closing brackets:
        step = brackets.closing.length;
        pos = 0;
        run = true;

        while( run ) {
            pos = content.indexOf( brackets.closing, pos );
            if( pos >= 0 ) {
                ( brackets.positions.closing as any )[bracketsCount.closing] = pos;
                bracketsCount.closing++;
                pos += step;
            }
            else {
                run = false;
            }
        }

        // Sort out the situation, log any pertinent information
        if( bracketsCount.opening > 0 && bracketsCount.closing > 0 ) {
            if( bracketsCount.opening !== bracketsCount.closing ) {
                if( bracketsCount.opening > bracketsCount.closing ){
                    console.log( 'Possible non-closed subscript detected. Opening: ' + bracketsCount.opening + ' Closing: ' + bracketsCount.closing );
                }
                else{
                    console.log( 'Possible non-opened subscript detected. Opening: ' + bracketsCount.opening + ' Closing: ' + bracketsCount.closing );
                }
            }
        }

        // Pull each top-level kwaeri block - which should contain all of its nested kwaeri blocks;
        // we will go in ascending numerical order to preserve any programmatic intent.
        let subscriptBlocks: any = [],
            count = 0;

        for( let i = 0; i < brackets.positions.opening.length; i++ ) {
            // We need to be careful to check for nested kwaeri blocks, prepare
            // some logic for this.
            let offset = 1,
                offset2 = 0;
            run = true;

            while( run ) {
                // If there are no additional brackets, the logic is pointless; continue
                if( ( i + offset ) > brackets.positions.opening.length )
                    break;

                // Otherwise, check that the next opening bracket's position is actually greater than
                // the current closing bracket's position (if not, the current closing bracket is likely
                // the closing bracket of said next opening bracket)
                if( brackets.positions.opening[(i + offset)] < brackets.positions.closing[i + offset2] ) {
                    // And so we'll modify our logic to properly check for the matching closing bracket
                    // position of the current opening bracket
                    offset++;
                    offset2++;
                }
                else {
                    // And when we find the proper closing bracket, let us continue
                    run = false;
                }
            }

            // We store the substring between the matching opening and closing brackets (which includes any nested kwaeri blocks)
            // so that we can do further testing. However, when we finally go with this new implementation we will be directly
            // replacing - or 'decorating' - the content of the string so as to properly format the response for the client.
            subscriptBlocks[count] = content.substr( brackets.positions.opening[i], ( ( brackets.positions.closing[(i + offset2)] + step ) - brackets.positions.opening[i] ) );

            // It's important we set the opening bracket's position we will use next, so that we are starting our next parse
            // after the current closing bracket's position.
            i += offset2;

            // And increment our separate count so that we properly extend the subscriptBlocks object with the next kwaeri block
            count++;
        }

        /* The following portions of code are for debugging purposes
        if( this.debug )
        {
            for( var i = 0; i < brackets.positions.opening.length; i ++ )
            {
                // View all of the bracket positions that were found, after the fact
                console.log( 'Opening bracket ' + i + ' position: ' + brackets.positions.opening[i] );
                console.log( 'Closing bracket ' + i + ' position: ' + brackets.positions.closing[i] );
            }

            // See how many we were able to pull out:
            //console.log( 'Kwaeri block identifiers counted: ' + bracketsCount.opening + ', ' + bracketsCount.closing );
            //console.log( 'Kwaeri subscript blocks parsed: ' + subscriptBlocks.length + ', ' + count );
        }
        */

        // Do further parsing, and decoration of each kwaeri block
        let o = content;
        for( let block in subscriptBlocks ) {
            o = o.replace
            (
                subscriptBlocks[block],
                () => {
                    // Prepare the subscriptBlock so our regex works properly
                    subscriptBlocks[block] = subscriptBlocks[block].substr( step, ( subscriptBlocks[block].length - ( step * 2 ) ) );

                    // First strip all comments
                    subscriptBlocks[block] = subscriptBlocks[block].replace(
                        /[^:"](\/\/.*)[\r\n]|(\/\*([^]*?)\*\/)/mg,
                        ( match: any, singline: any, multiline: any, extra: any ) => {
                            return "";
                        }
                    );

                    // Then handle the subscript
                    subscriptBlocks[block] = subscriptBlocks[block].replace(
                        /^[\r\n\s]*(([\w_]+[\r\n\s]??)+?(\.[\w_]+[\r\n\s]*)*(\(([^]*?)\))?([\r\n\s]*(\{([^]*)\})?([\r\n\s]*else[\r\n\s]*\{([^]*)\})*?))/,
                        ( match: any, subscript: any, fof: any, memstring: any, argflow: any, args: any, ssflow: any, ssstring: any, superscript: any, ebflow: any, extrablock: any ) => {
                            // Clear up any leading/trailing spaces in fof
                            fof = fof.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                            //console.log( 'Function or Factory: "' + fof + ( memstring || '' ) + '"' );

                            // Same with memstring if present
                            if( memstring )
                                memstring = memstring.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                            // And same with args if present
                            if( args )
                                args = args.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                            if( fof === 'partial' ) {
                                if( args ) {
                                    let pdata = args;
                                    pdata = pdata.replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                                    console.log( "Partial layout rendering not currently supported" );
                                    //return processor.parse( processor.partial( pdata ), klay );
                                }
                                return "";
                            }

                            if( fof === 'if' ) {
                                let argument = args.replace( /[\s\r\n]*/g, '' );

                                let pieces = ssflow.split( "else" );
                                if( pieces.length > 1 ) {
                                    // Here we need to apply logic for filtering iterated parses
                                    let status;
                                    let covered = false;
                                    if( iteration  ) {
                                        if( identification ) {
                                            if( klay.viewbag.hasOwnProperty( identification ) ) {
                                                if( klay.viewbag[identification as string].length >= iteration ) {
                                                    let targ = argument.split( '.' );
                                                    if( _.type( targ ) === 'array' && targ.length > 0 ) {
                                                        if( klay.viewbag[identification as string][iteration as number].hasOwnProperty( targ[0] ) ) {
                                                            if( klay.viewbag[identification as string][iteration as number][targ[0]].hasOwnProperty( targ[1] ) ) {
                                                                if( klay.viewbag[identification as string][iteration as number][targ[0]][targ[1]] ) {
                                                                    status = true;
                                                                    covered = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        if( klay.viewbag[identification as string][iteration as number].hasOwnProperty( argument ) ) {
                                                            if( klay.viewbag[identification as string][iteration as number][argument] ) {
                                                                status = true;
                                                                covered = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }


                                    if( klay.viewbag.hasOwnProperty( argument ) || klay.hasOwnProperty( argument ) || covered === true ) {
                                        if( argument === 'authenticated' ) {
                                            if( klay[argument] )
                                                status = true;
                                        }
                                        else {
                                            if( !covered ) {
                                                let targ = argument.split( '.' );
                                                if( _.type( targ ) === 'array' && targ.length > 0 ) {
                                                    if( klay.viewbag.hasOwnProperty( targ[0] ) ) {
                                                        if( targ.length > 1 && klay.viewbag[targ[0]].hasOwnProperty( targ[1] ) ) {
                                                            if( klay.viewbag[targ[0]][targ[1]] ) {
                                                                status = true;
                                                                covered = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                else {
                                                    if( klay.viewbag[argument] )
                                                        status = true;
                                                }
                                            }
                                        }

                                        if( status ) {
                                            if( !pieces[0] )
                                                return '';

                                            pieces[0] = pieces[0].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                            if( iteration )
                                                if( identification )
                                                    return processor.parse( pieces[0], klay, iteration, identification );
                                                else
                                                    return processor.parse( pieces[0], klay, iteration );
                                            else
                                                return processor.parse( pieces[0], klay );
                                        }
                                        else {
                                            if( !pieces[1] )
                                                return '';

                                            pieces[1] = pieces[1].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                            if( iteration )
                                                if( identification )
                                                    return processor.parse( pieces[1], klay, iteration, identification );
                                                else
                                                    return processor.parse( pieces[1], klay, iteration );
                                            else
                                                return processor.parse( pieces[1], klay );
                                        }
                                    }
                                    else {
                                        if( !pieces[1] )
                                            return '';

                                        pieces[1] = pieces[1].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g, '' );

                                        if( iteration )
                                            if( identification )
                                                return processor.parse( pieces[1], klay, iteration, identification );
                                            else
                                                return processor.parse( pieces[1], klay, iteration );
                                        else
                                            return processor.parse( pieces[1], klay );
                                    }
                                }
                                else {
                                    // Here we need to apply logic for filtering iterated parses
                                    let status;
                                    let covered = false;
                                    if( iteration  ) {
                                        if( identification ) {
                                            if( klay.viewbag.hasOwnProperty( identification ) ) {
                                                if( klay.viewbag[identification as string].length >= iteration ) {
                                                    var targ = argument.split( '.' );
                                                    if( _.type( targ ) === 'array' && targ.length > 0 ) {
                                                        if( klay.viewbag[identification as string][iteration as number].hasOwnProperty( targ[0] ) ) {
                                                            if( klay.viewbag[identification as string][iteration as number][targ[0]].hasOwnProperty( targ[1] ) ) {
                                                                if( klay.viewbag[identification as string][iteration as number][targ[0]][targ[1]] ) {
                                                                    status = true;
                                                                    covered = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        if( klay.viewbag[identification as string][iteration as number].hasOwnProperty( argument ) ) {
                                                            if( klay.viewbag[identification as string][iteration as number][argument] ) {
                                                                status = true;
                                                                covered = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if( klay.viewbag.hasOwnProperty( argument ) || klay.hasOwnProperty( argument ) || covered === true ) {
                                        if( !pieces[0] )
                                            return '';

                                        if( argument === 'authenticated' )
                                            if( klay[argument] )
                                                status = true;
                                        else
                                            if( !covered ) {
                                                let targ = argument.split( '.' );
                                                if( _.type( targ ) === 'array' && targ.length > 0 )
                                                    if( klay.viewbag.hasOwnProperty( targ[0] ) )
                                                        if( targ.length > 1 && klay.viewbag[targ[0]].hasOwnProperty( targ[1] ) )
                                                            if( klay.viewbag[targ[0]][targ[1]] ) {
                                                                status = true;
                                                                covered = true;
                                                            }
                                                else
                                                    if( klay.viewbag[argument] )
                                                        status = true;
                                            }

                                        if( !status )
                                            return '';

                                        pieces[0] = pieces[0].replace( /^[\s\r\n]*\{|\}?[\s\r\n]*$/g,'' );

                                        if( iteration )
                                            if( identification )
                                                return processor.parse( pieces[0], klay, iteration, identification );
                                            else
                                                return processor.parse( pieces[0], klay, iteration );
                                        else
                                            return processor.parse( pieces[0], klay );
                                    }
                                    else
                                        return '';

                                }

                                return 'Error: Could not find an associate code block for this [if] statement.';
                            }

                            if( fof === 'foreach' ) {
                                let found       = false,
                                    pieces: boolean|any      = false,
                                    iterator: boolean|any    = false,
                                    identifier: boolean|any  = false;

                                if( args ) {
                                    pieces = args.split( " as " );

                                    if( ( pieces as any ).length > 1 ) {
                                        // User has specified '..as <identifier>'
                                        iterator = pieces[0];
                                        identifier = pieces[1];
                                    }
                                    else {
                                        iterator = pieces[0];
                                    }

                                    // The iterator may be a variable with property specifier
                                    let iteratorPieces = ( iterator as any ).split( '.' );
                                    if( _.type( iteratorPieces ) === 'array' && iteratorPieces.length >= 1 )
                                        iterator = iteratorPieces;

                                }

                                // Strip all white space
                                if( identifier )
                                    identifier = ( identifier as any ).replace( /^[\s\r\n]*|[\s\r\n]*$/g, '' );

                                // Now, for each member of our array or object, we want to recursively run some scripts.  There could
                                // also be plain html, which should be copied and values parsed for each iteration.  Everything within
                                // superscript should be run/copied for each iteration, and should be passed only the member/record that
                                // the iteration pertains to.
                                let so = "";

                                // See if iterator is an array
                                if( _.type( iterator ) === 'array' && ( iterator as any ).length > 0 ) {
                                    if( klay.viewbag.hasOwnProperty( iterator[0] ) || klay.hasOwnProperty( iterator[0] ) ) {
                                        let puppet;
                                        if( iterator[0] === 'modules' )
                                            puppet = klay;
                                        else
                                            puppet = klay.viewbag;

                                        let complexity = ( iterator as any ).length;
                                        let check_was_good = true;

                                        for( let i = 0; i <= complexity; i++ ) {
                                            if( check_was_good )
                                                if( puppet.hasOwnProperty( iterator[i] ) )
                                                    puppet = puppet[iterator[i]];
                                                else
                                                    check_was_good = false;
                                            else
                                                console.log( '[Renderer] _foreach_ Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                        }

                                        if( _.type( puppet ) === 'array' && puppet.length > 0 )
                                            for( let i = 0; i < puppet.length; i++ ) {
                                                if( identifier )
                                                    so += processor.parse( superscript, klay, puppet[i], identifier );
                                                else
                                                    so += processor.parse( superscript, klay, puppet[i], false );
                                            }
                                        else
                                            if( identifier )
                                                so += processor.parse( superscript, klay, puppet, identifier );
                                            else
                                                so += processor.parse( superscript, klay, puppet, false );

                                        return so;
                                    }
                                }
                                else {
                                    if( klay.viewbag.hasOwnProperty( iterator ) ) {
                                        for( let record in klay.viewbag[iterator as any] ) {
                                            // We pass the individual record as well as the entire contents of the foreach call, to be parsed
                                            // by the processor.  By passing the individual record, our processor will know to use the passed
                                            // record for values within
                                            if( identifier )
                                                so += processor.parse( superscript, klay, klay.viewbag[iterator as any][record], identifier );
                                            else
                                                so += processor.parse( superscript, klay, klay.viewbag[iterator as any][record], false );
                                        }

                                        return so;
                                    }
                                }

                                return so;
                            }

                            if( fof === 'html' ) {
                                // Here we need to apply logic for filtering iterated parses
                                if( iteration  )
                                    if( identification )
                                        return processor.decorate( fof, args, klay, memstring || "", iteration, identification );
                                    else
                                        return processor.decorate( fof, args, klay, memstring || "", iteration, false );
                                else
                                    return processor.decorate( fof, args, klay, memstring || "", false, false );
                            }

                            if( fof === 'modules' ) {
                                let position = args.replace( /[\s\r\n]*/g, '' );

                                if( klay.hasOwnProperty( 'modules' ) && klay.modules.hasOwnProperty( 'rendered' ) ) {
                                    let property = klay.modules.rendered;
                                    let status = false;

                                    if( property.hasOwnProperty( position ) )
                                        status = true;

                                    if( status ) {
                                        if( !ssflow )
                                            return '';

                                        let moduleContent = ssflow;
                                        moduleContent = moduleContent.replace(
                                            /^[\s\r\n]*\{([^]*)\}[\s\r\n]*$/,
                                            ( match: any, capture: any ) => {
                                                return capture;
                                            }
                                        );

                                        return processor.parse( moduleContent, klay );
                                    }
                                    else {
                                        if( !ssflow )
                                            return '';

                                        return ssflow;
                                    }
                                }
                                else{
                                    if( !ssflow )
                                        return '';

                                    return processor.parse( ssflow, klay );
                                }

                                return ssflow || '';
                            }

                            if( iteration )
                                if( identification )
                                    return processor.decorate( fof, subscript, klay, memstring || "", iteration, identification );
                                else
                                    return processor.decorate( fof, subscript, klay, memstring || "", iteration, false );
                            else
                                return processor.decorate( fof, subscript, klay, memstring || "", false, false );
                        }
                    );

                    return subscriptBlocks[block];
                }
            );
        }

        return o;
    };


    /**
     * Returns the result of a subscript
     *
     * @param fof
     * @param args
     * @param klay
     * @param memstring
     * @param iteration
     * @param identification
     */
    decorate( fof: any, args: any, klay: any, memstring: any, iteration: any, identification: any ) {
        let processor = this;

        if( !iteration )
            iteration = false;

        if( !identification )
            identification = false;

        // Let's breakdown the function or factory
        let nested = false,
            buffered = '',
            fmembs = fof.split( '.' );
        if( fmembs.length > 1 ) {
            nested = true;
            buffered = fmembs[0];
        }
        else {
            fmembs = memstring.split( '.' );
            if( fmembs.length > 1 )
                nested = true;

            buffered = fof;
        }

        // Returns the output of the subscript
        switch( buffered ) {
            case 'test': {
                return 'Test replacement worked!';
            } break;

            case 'title': {
                return klay.viewbag.title;
            } break;

            case 'links': {
                let o = '';
                if( klay.hasOwnProperty( 'links' ) )
                    if( klay.links.length > -1 )
                        for( let i = 0; i < klay.links.length; i++ ) {
                            if( i === 0 )
                                o = klay.links[i] + '\n';
                            else
                                o += '        ' + klay.links[i] + '\n';
                        }

                return o;
            } break;

            case 'pagetitle': {
                return klay.viewbag.pagetitle;
            } break;

            case 'body': {
                return klay.view;
            } break;

            case 'scripts': {
                let o = '';
                if( klay.hasOwnProperty( 'scripts' ) )
                    if( klay.scripts.length > -1 )
                        for( let i = 0; i < klay.scripts.length; i++ ) {
                            if( i === 0 )
                                o = klay.scripts[i] + '\n';
                            else
                                o += '        ' + klay.scripts[i] + '\n';
                        }

                return o;
            } break;

            case 'user': {
                // We can allow any depth:
                if( klay.viewbag.hasOwnProperty( 'user' ) ) {
                    let complexity = fmembs.length;
                    let puppet: any = [];
                    let check_was_good = true;

                    // Build the puppet heiarchy
                    for( let i = 1; i < complexity; i++ ) {
                        if( check_was_good )
                            if( i === 1 )
                                if( klay.viewbag.user.hasOwnProperty( fmembs[i] ) )
                                    puppet = klay.viewbag.user[fmembs[i]];
                                else
                                    check_was_good = false;
                            else
                                if( puppet.hasOwnProperty( fmembs[i] ) )
                                    puppet = puppet[fmembs[i]];
                                else
                                    check_was_good = false;
                        else
                            console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                            break;
                    }

                    if( check_was_good )
                        return puppet;
                }
            } break;

            // This entire case will be pulled into an extension before long, as with much of this files functionality.
            case 'gravatar': {
                let user = '',
                    email = '';

                let gravatar = true;
                if( klay.viewbag.hasOwnProperty( 'user' ) ) {
                    if( klay.viewbag.user.hasOwnProperty( 'username' ) )
                        user = klay.viewbag.user.username;
                    else
                        gravatar = false;
                        user = 'Guest';

                    if( klay.viewbag.user.hasOwnProperty( 'email' ) )
                        email = klay.viewbag.user.email;
                    else
                        email = "email@example.com";
                        gravatar = false;
                }
                else {
                    gravatar = false;
                    user = 'guest';
                    email = 'email@example.com';
                }

                let img_tag = '';
                if( gravatar ) {
                    let hash = '';
                    hash = ( _ as any ).hash_md5( email );
                    img_tag = '<img src="//www.gravatar.com/avatar/' + hash + '" alt="' + user + '" class="img-circle mmod-user-icon" />';
                }
                else
                    img_tag = '<img src="/images/default_user_icon.png" alt="' + user + '" class="img-circle mmod-user-icon" />';

                return img_tag;

            } break;

            case 'date': {
                let o: any = '';
                switch( fmembs[1] ) {
                    case 'year': {
                        o = this.date.getFullYear();
                    } break;
                }

                return this.date.getFullYear();
            }break;

            case 'model': {
                return 'model';
            } break;

            case 'html': {
                let rtools = processor.utilities,
                subscript = args,
                rargs: any = [],
                control = 0;

                // Strip our arguments
                subscript = subscript.replace(   //(((([a-zA-Z0-9_]*)(\.([a-zA-Z0-9_]*))*(\((.*)\))?[\}]?)|([\{](.*)\}))[\,]?)*/mg,
                    /(((([a-zA-Z0-9_]*)(\.([a-zA-Z0-9_]*))*(\((.*)\))?)|(\"\")|([\"](.*)[\"]?)[\,]?|([\{](.*)[\}]?))[\,]?)*/mg,
                    ( match: any, match2: any, match3: any, noobjarg: any, method: any, memflow: any, member: any, argflow: any, argstring: any, objarg: any, objcontents: any  ) => {
                        if( match3 ) {
                            rargs[control] = match3;
                            control++;
                        }
                        return "";
                    }
                );

                // The memstring contained .<whatever members were present after html>, let's split it
                let members = memstring.split( '.' );
                //console.log( members[0] + ' - ' + members[1] );

                // Make sure the rendering tools support the requested method
                if( this.utilities._classmap.hasOwnProperty( members[1] ) ) {
                    let memberparts = rargs[0].split( '.' );
                    if( memberparts[0] === 'model' ) {   // We'll grab the display text value from the model's schema
                        if( memberparts[1] )
                            if( klay.model.schema.hasOwnProperty( memberparts[1] ) )
                                rargs[0] = [ memberparts[1], klay.model ];
                            else {
                                console.log( 'Error: Unknown member: `[Model].' + memberparts[1] + '`.' );
                                rargs[0] = [ false, 'Unknown member:  `[Model].' + memberparts[1] + '`.' ];
                            }
                        else
                            rargs[0] = [ false, 'Unknown member:  `[Model].' + memberparts[1] + '`.' ];
                    }
                    else {   // We'll literally pass what is requested from the viewbag
                        if( klay.viewbag ) {
                            let pcnt = 0;
                            if( _.type( memberparts ) === 'array' )
                                pcnt = memberparts.length;

                            // We can support any depth here
                            if( pcnt == 0 ) {
                                if( klay.viewbag.hasOwnProperty( memberparts ) )
                                    rargs[0] = [ memberparts, klay.viewbag[memberparts] ];
                                else
                                    if( memberparts === identification && iteration )
                                        rargs[0] = [ memberparts, iteration ];
                                    else
                                        rargs[0] = [ false, memberparts ];      // Text value
                            }
                            else {
                                if( klay.viewbag.hasOwnProperty( memberparts[0] ) ) {
                                    let complexity = memberparts.length;
                                    let puppet = klay.viewbag[memberparts[0]];
                                    let current_property = memberparts[0];
                                    let check_was_good = true;

                                    for( let i = 1; i < complexity; i++ ) {
                                        if( check_was_good ) {
                                            current_property = memberparts[i];
                                            if( puppet.hasOwnProperty( memberparts[i] ) )
                                                puppet = puppet[memberparts[i]];
                                            else
                                                check_was_good = false;
                                        }
                                        else {
                                            console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                            rargs[0] = [ false, memberparts.join( '' ) ];
                                        }
                                    }

                                    rargs[0] = [ current_property, puppet ];
                                }
                                else {
                                    if( ( memberparts[0] === identification ) && iteration ) {
                                        let complexity = memberparts.length;
                                        let puppet = iteration;
                                        let current_property = identification;
                                        let check_was_good = true;

                                        for( let i = 1; i < complexity; i++ ) {
                                            if( check_was_good ) {
                                                current_property = memberparts[i];
                                                if( puppet.hasOwnProperty( memberparts[i] ) )
                                                    puppet = puppet[memberparts[i]];
                                                else
                                                    check_was_good = false;
                                            }
                                            else {
                                                console.log( '[Renderer] Error: Reiterative check failed at current depth ' + i + ' of ' + complexity + ' total.' );
                                                rargs[0] = [ false, memberparts.join( '' ) ];
                                            }
                                        }
                                        rargs[0] = [ current_property, puppet ];
                                    }
                                    else
                                        rargs[0] = [ false, memberparts[0] ];
                                }
                            }
                        }
                        else {
                            // Text value
                            if( memberparts[0] )
                                rargs[0] = [ memberparts[0], memberparts[0] ];
                            else
                                rargs[0] = [ false, 'whoops' ];
                        }
                    }

                    // And finally we invoke the requested method and pass it any arguments that it needs to return a string
                    if( rargs[0] && rargs[1] ) {
                        // Two args provided, parse second argument as a JSON string representation of an object
                        rargs[1] = JSON.parse( rargs[1] );
                        subscript = rtools.generate( members[1], rargs[0], rargs[1] );
                    }
                    else if( rargs[0] ) {
                        // One arg provided
                        subscript = rtools.generate( members[1], rargs[0] );
                    }
                    else {
                        // No args provided
                        subscript = rtools.generate( members[1] );
                    }
                }
                else {
                    // There just weren't any members which matched
                    console.log( 'Error: Invalid script `html.' + members[1] + '`.' );
                    subscript = "";
                }

                return String( subscript );
            } break;

            default: {
                if( iteration ) {
                    if( identification ) {
                        if( fof ) {
                            // We're already iterating over a viewbag item, the identification let's us know what word was used to signify the current iteration.
                            if( fof === identification ) {
                                if( fmembs[1] && iteration.hasOwnProperty( fmembs[1] ) )
                                    return iteration[fmembs[1]];
                                else {
                                    if( iteration && !fmembs[1] ) {
                                        // It's possible its pre-rendered content, so if fmembs[1] is not defined
                                        // let's return the iteration itself
                                        if( _.type( iteration ) === 'string' )
                                            return iteration;
                                    }

                                    return 'Does not exist: ' + iteration + '[' + fmembs[1] + ']';
                                }
                            }
                        }

                        return '';
                    }
                    else
                        return 'Error: Identification missing for this iteration of the invoked script provided in this kwaeri block.';
                }
                else {
                    if( fof )
                        return klay.viewbag[fof];

                    return '';
                }
            } break;
        }

        // Enable this and disable the latter, for testing.
        //return 'Too bad...';
        return '';
    };
}